/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import javax.xml.bind.annotation.*; 

/**
 *
 * @author raudi
 */
@XmlRootElement(name = "accountObject") 
public class accountObject { 
    @XmlElement(name="id", required=true) 
    private int id; 
    @XmlElement(name="fullname", required=true) 
    private String fullname;
    @XmlElement(name="username", required=true) 
    private String username;
    @XmlElement(name="address", required=true) 
    private String address;
    @XmlElement(name="postalcode", required=true) 
    private String postalcode; 
    @XmlElement(name="phonenumber", required=true) 
    private String phonenumber;
    @XmlElement(name="token", required=true) 
    private String token;
    @XmlElement(name="validate", required=true) 
    private int validate;
    
    public accountObject() { 
        id = 0;
        fullname = ""; 
        username = ""; 
        address = ""; 
        postalcode = "";
        phonenumber = "";
        token = "";
        validate = 0;
    } 
    
    public accountObject(int id, String fullname, String username, String address,
                           String postalcode, String phonenumber, String token, int validate) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.address = address;
        this.postalcode = postalcode;
        this.phonenumber = phonenumber;
        this.token = token;
        this.validate = validate;
    }
    
    public int getId() {
        return id;
    }
    
    public String getFullname(){
        return fullname;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getAddress() {
        return address;
    }
    
    public String getPostalcode() {
        return postalcode;
    }
    
    public String getPhonenumber() {
        return phonenumber;
    }
    
    public String getToken() {
        return token;
    }
    
    public int getValidate() {
        return validate;
    }
}
