/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package item;

import javax.xml.bind.annotation.*; 

/**
 *
 * @author raudi
 */
@XmlRootElement(name = "itemObject") 
public class itemObject { 
    @XmlElement(name="id", required=true) 
    public int id; 
    @XmlElement(name="name", required=true) 
    public String name; 
    @XmlElement(name="description", required=true) 
    public String description; 
    @XmlElement(name="price", required=true) 
    public int price; 
    @XmlElement(name="photo", required=true) 
    public String photo;
    @XmlElement(name="seller", required=true) 
    public int seller;
    @XmlElement(name="date", required=true) 
    public String date;
    @XmlElement(name="time", required=true) 
    public String time;
    
    public itemObject() { 
        id = 0; 
        name = ""; 
        description = "";
        price = 0;
        photo = "";
        seller = 0;
        date = "";
        time = "";
    } 
    
    public itemObject(int id, String name, String description, int price, String photo, int seller, String date, String time) { 
        this.id = id; 
        this.name = name;
        this.description = description;
        this.price = price;
        this.photo = photo;
        this.seller = seller;
        this.date = date;
        this.time = time;
    }
    
    public int getID() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public int getPrice() {
        return price;
    }
    
    public int getSeller() {
        return seller;
    }
    
    public String getDate() {
        return date;
    }
    
    public String getTime() {
        return time;
    }

}