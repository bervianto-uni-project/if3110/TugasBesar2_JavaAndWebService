/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buys;

import javax.xml.bind.annotation.*; 

/**
 *
 * @author raudi
 */
@XmlRootElement(name = "buysObject") 
public class buysObject { 
    @XmlElement(name="id", required=true) 
    private int id; 
    @XmlElement(name="id_item", required=true) 
    private int id_item;
    @XmlElement(name="id_buyer", required=true) 
    private int id_buyer;
    @XmlElement(name="id_seller", required=true) 
    private int id_seller;
    @XmlElement(name="photo", required=true) 
    private String photo; 
    @XmlElement(name="item_name", required=true) 
    private String item_name;
    @XmlElement(name="item_price", required=true) 
    private int item_price;
    @XmlElement(name="quantity", required=true) 
    private int quantity; 
    @XmlElement(name="consignee", required=true) 
    private String consignee; 
    @XmlElement(name="full_address", required=true) 
    private String full_address;
    @XmlElement(name="postal_code", required=true) 
    private String postal_code;
    @XmlElement(name="phone_number", required=true) 
    private String phone_number;
    @XmlElement(name="cc_number", required=true) 
    private String cc_number;
    @XmlElement(name="verification_code", required=true) 
    private String verification_code;
    @XmlElement(name="date", required=true) 
    private String date;
    @XmlElement(name="time", required=true) 
    private String time;
    
    public buysObject() { 
        id = 0;
        id_item = 0; 
        id_buyer = 0; 
        id_seller = 0; 
        photo = "";
        item_name = ""; 
        item_price = 0;
        quantity = 0;
        consignee = "";
        full_address = "";
        postal_code = "";
        phone_number = "";
        cc_number = "";
        verification_code = "";
        date = "";
        time = "";
    } 
    
    public buysObject(int id, int id_item, int id_buyer, int id_seller,
                      String photo, String item_name, int item_price, int quantity,
                      String consignee, String full_address, String postal_code, String phone_number,
                      String cc_number, String verification_code, String date, String time) { 
        this.id = id;
        this.id_item = id_item;
        this.id_buyer = id_buyer;
        this.id_seller = id_seller;
        this.photo = photo;
        this.item_name = item_name;
        this.item_price = item_price;
        this.quantity = quantity;
        this.consignee = consignee;
        this.full_address = full_address;
        this.postal_code = postal_code;
        this.phone_number = phone_number;
        this.cc_number = cc_number;
        this.verification_code = verification_code;
        this.date = date;
        this.time = time;
    }
    
    public int getID() {
        return id;
    }
    
    public int getIDItem() {
        return id_item;
    }
    
    public int getIDBuyer() {
        return id_buyer;
    }
    
    public int getIDSeller() {
        return id_seller;
    }
    
    public String getPhoto() {
        return photo;
    }
    
    public String getItemName() {
        return item_name;
    }
    
    public int getItemPrice() {
        return item_price;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public String getConsignee() {
        return consignee;
    }
    
    public String getFullAddress() {
        return full_address;
    }
    
    public String getPostalCode() {
        return postal_code;
    }
    
    public String getPhoneNumber() {
        return phone_number;
    }
    
    public String getCCNumber() {
        return cc_number;
    }
    
    public String getVerificationCode() {
        return verification_code;
    }
    
    public String getDate() {
        return date;
    }
    
    public String getTime() {
        return time;
    }

}