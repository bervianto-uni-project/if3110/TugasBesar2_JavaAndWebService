<%-- 
    Document   : puchases
    Created on : Nov 8, 2016, 10:52:47 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<!DOCTYPE HTML> 
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
	account.AccountObject resultValid = portValid.validate(token);
	if (resultValid.getValidate() == 2) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else if (resultValid.getValidate() == 3) {
            response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
        }
    %>
	<head> 
		<title>Purchases</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css"> 
	</head> 
	<body id="body-color"> 

		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
			<ul>
				<li><a href="catalog.jsp">Catalog</a></li>
				<li><a href="your_product.jsp">Your Products</a></li>
				<li><a href="add_product.jsp">Add Products</a></li>
				<li><a href="sales.jsp">Sales</a></li>
				<li><a class="active" href="purchases.jsp">Purchases</a></li>
			</ul>
			<br><br>
		</div>
		<div>
			<h2> Here are your purchases </h2>
		</div>
		    <%-- start web service invocation --%>
                    <%
                        buys.Buys_Service service = new buys.Buys_Service();
                        buys.Buys port = service.getBuysPort();
                         // TODO initialize WS operation arguments here
                        int buyerID = resultValid.getId();
                        // TODO process result here
                        java.util.List<buys.BuysObject> result = port.purchases(buyerID);
                        
                        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                        for(int i=0;i<result.size();i++) {
                    %>

		<div class="filter">
			<!-- HARUSNYA ADA TANGGAL DI BUYS -->
			<div class="product-view">
				<% out.print(result.get(i).getDate()); %> <br> at <% out.print(result.get(i).getTime()); %>
			</div>
			<div class="product-view">
				<div class="photo">
					<img src=<% out.print(result.get(i).getPhoto()); %> alt="Mountain View" width="100px" height="100px">
				</div>
				<div class="description-sales">
					<b><% out.print(result.get(i).getItemName()); %></b><br>
					<% out.print(defaultFormat.format(result.get(i).getQuantity()*result.get(i).getItemPrice()));%><br>
					<% out.print(result.get(i).getQuantity()); %> pcs<br>
					@<% out.print(defaultFormat.format(result.get(i).getItemPrice())); %><br>
					<br>
                                        <font size='1'>bought from <b><%-- start web service invocation --%>
                                                                        <%
                                                                            account.Accounts portUsername = serviceValid.getAccountsPort();
                                                                             // TODO initialize WS operation arguments here
                                                                            int id = result.get(i).getIdSeller();
                                                                            // TODO process result here
                                                                            java.lang.String resultUsername = portUsername.getUsername(id);
                                                                            out.println(resultUsername);
                                                                        %>
                                                                        <%-- end web service invocation --%></b></font>
				</div>
				<div class="detail-sales">
					Delivery to <b><% out.print(result.get(i).getConsignee()); %></b><br>
					<% out.print(result.get(i).getFullAddress()); %><br>
					<% out.print(result.get(i).getPostalCode()); %><br>
					<% out.print(result.get(i).getPhoneNumber()); %><br>
				</div>
			</div>
		</div>
                <% } %>
	</body>
</html>

