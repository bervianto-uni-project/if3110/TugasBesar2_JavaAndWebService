			function validateSignIn() {
				validateUsername();
				validatePass();
				return validateUsername() && validatePass();
			}
			
			function validateRegister() {
				validateFullName();
				validateUsername();
				validateEmail();
				validatePass();
				validateConfirmPass();
				validateFullAddress();
				validatePostalCode();
				validatePhoneNumber();
				return validateEmail() && validateConfirmPass() && validateFullName() && validatePass() && validateFullAddress() && validatePostalCode() && validatePhoneNumber();
			}
			
			function validatePurchase() {
				validateQuantity();
				validateConsignee();
				validateFullAddress();
				validatePostalCode();
				validatePhoneNumber();
				validateCCNumber();
				validateCCValue();
				if (validateQuantity() && validateFullName() && validateFullAddress() &&validatePostalCode() && validatePhoneNumber() && validateCCNumber() && validateCCValue()) {
					if (confirm("Apakah data yang anda masukan benar?") == true) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			
			function validateAddProduct() {
				validateName();
				validateDescription();
				validatePrice();
				validatePhoto();
				return validateName() && validateDescription() && validatePrice() && validatePhoto();
			}
			
			function validateEditProduct() {
				validateName();
				validateDescription();
				validatePrice();
				return validateName() && validateDescription() && validatePrice();
			}
			
			function validateEmail() {
				var inName = "email";
				var kosong = document.getElementById('isinotif-' + inName);

				if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.getElementById(inName).value) && (document.getElementById(inName).value!="")) {
					document.getElementById(inName).style.borderWidth="thin";
					document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
					if (kosong!=null) {
						kosong.innerHTML = '';
					}	
					return true;
				} else {
					document.getElementById(inName).style.borderColor="red";
					document.getElementById(inName).style.borderWidth="medium";
					if (kosong!=null) {

					}
					else {
						var div = document.createElement('div');
						div.id = 'isinotif-' + inName;
						div.innerHTML = '<div style="color:red;">* Incorrect email format!</div>';
						document.getElementById('notif-' + inName).appendChild(div);
					}
					return false;
				}
			}
			
			function validateConfirmPass() {
				var password = "password";
				var confirmPassword = "confirmpassword";
				var kosong = document.getElementById('isinotif-' + confirmPassword);

				if ((document.getElementById(password).value==document.getElementById(confirmPassword).value)&& (document.getElementById(password).value!="") && (document.getElementById(confirmPassword).value!="")) {
					document.getElementById(confirmPassword).style.borderWidth="thin";
					document.getElementById(confirmPassword).style.borderColor="#66afe9";
					if (kosong!=null) {
						kosong.innerHTML = '';
					}			
					return true;
				}
				else {
					document.getElementById(confirmPassword).style.borderColor="red";
					document.getElementById(confirmPassword).style.borderWidth="medium";
					if (kosong!=null) {

					}
					else {
						var div = document.createElement('div');
						div.id = 'isinotif-' + confirmPassword;
						div.innerHTML = '<div style="color:red;">* Password not match!</div>';
						document.getElementById('notif-' + confirmPassword).appendChild(div);
					}
					
					return false;
				}
			}
			
			
			function validateFullName() {
				var inName = "fullname";
				var kosong = document.getElementById('isinotif-' + inName);

				if (document.getElementById(inName).value!="") {
					document.getElementById(inName).style.borderWidth="thin";
					document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
					if (kosong!=null) {
						kosong.innerHTML = '';
					}			
					return true;
				}
				else {
					document.getElementById(inName).style.borderColor="red";
					document.getElementById(inName).style.borderWidth="medium";
					if (kosong!=null) {

					}
					else {
						var div = document.createElement('div');
						div.id = 'isinotif-' + inName;
						div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
						document.getElementById('notif-' + inName).appendChild(div);
					}
					
					return false;
				}
			}
			
			function validateConsignee() {
				var inName = "consignee";
				var kosong = document.getElementById('isinotif-' + inName);

				if (document.getElementById(inName).value!="") {
					document.getElementById(inName).style.borderWidth="thin";
					document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
					if (kosong!=null) {
						kosong.innerHTML = '';
					}			
					return true;
				}
				else {
					document.getElementById(inName).style.borderColor="red";
					document.getElementById(inName).style.borderWidth="medium";
					if (kosong!=null) {

					}
					else {
						var div = document.createElement('div');
						div.id = 'isinotif-' + inName;
						div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
						document.getElementById('notif-' + inName).appendChild(div);
					}
					
					return false;
				}
			}
			
			function validateUsername() {
				var inName = "username";
				var kosong = document.getElementById('isinotif-' + inName);

				if (document.getElementById(inName).value!="") {
					document.getElementById(inName).style.borderWidth="thin";
					document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
					if (kosong!=null) {
						kosong.innerHTML = '';
					}			
					return true;
				}
				else {
					document.getElementById(inName).style.borderColor="red";
					document.getElementById(inName).style.borderWidth="medium";
					if (kosong!=null) {

					}
					else {
						var div = document.createElement('div');
						div.id = 'isinotif-' + inName;
						div.innerHTML = '<div style="color:red;">* Please fill the field correctly!</div>';
						document.getElementById('notif-' + inName).appendChild(div);
					}
					
					return false;
				}
			}
			
			function validatePass() {
    		var inName = "password";
    		var kosong = document.getElementById('isinotif-' + inName);

			if (document.getElementById(inName).value!="") {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
        		if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
    		else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
		
		function validateFullAddress() {
    		var inName = "fullAddress";
            var kosong = document.getElementById('isinotif-' + inName);
			
			if (document.getElementById(inName).value!="") {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style	
        		if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
				return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
		
		function validatePostalCode() {
    		var inName = "postalCode";
			var kosong = document.getElementById('isinotif-' + inName);
			
   			if (document.getElementById(inName).value!="") {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style  
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	    			
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
		
		function validatePhoneNumber() {
    		var inName = "phoneNumber";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (document.getElementById(inName).value!="") {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
		
		function validateCCNumber() {
    		var inName = "ccNumber";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (/^\d+$/.test(document.getElementById(inName).value) && document.getElementById(inName).value.length==12) {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
		
		function validateCCValue() {
    		var inName = "verificationCode";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (/^\d+$/.test(document.getElementById(inName).value) && document.getElementById(inName).value.length==3) {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
    	
    	function validateName() {
    		var inName = "name";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (document.getElementById(inName).value!="") {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {	

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
    	
		function validateQuantity() {
			var inName = "quantity";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (/^\d+$/.test(document.getElementById(inName).value)) {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
		}
		
    	function validatePrice() {
			var inName = "price";
			var kosong = document.getElementById('isinotif-' + inName);
			if (/^\d+$/.test(document.getElementById(inName).value) && (document.getElementById(inName).value!="")) {
				document.getElementById(inName).style.borderWidth="thin";
				document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
					kosong.innerHTML = '';
				}	
				return true;
			} 
			else if ((document.getElementById(inName).value!="") == false) {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {	

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
			else if ((/^\d+$/.test(document.getElementById(inName).value))==false){
				document.getElementById(inName).style.borderColor="red";
				document.getElementById(inName).style.borderWidth="medium";
				if (kosong!=null) {

				}
				else {
					var div = document.createElement('div');
					div.id = 'isinotif-' + inName;
					div.innerHTML = '<div style="color:red;">* Incorrect price format!</div>';
					document.getElementById('notif-' + inName).appendChild(div);
				}
				return false;
			}
		}
		
		function validateDescription() {
    		var inName = "description";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (document.getElementById(inName).value!="") {
				if (document.getElementById(inName).value.length<=200) {
      				document.getElementById(inName).style.borderWidth="thin";
    				document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
					if (kosong!=null) {
        	    		kosong.innerHTML = '';
      		  		}	
    	    		return true;
    			}
				else {
        			document.getElementById(inName).style.borderColor="red";
        			document.getElementById(inName).style.borderWidth="medium";
        			if (kosong!=null) {
						
        			}
        			else {
           		 		var div = document.createElement('div');
           				div.id = 'isinotif-' + inName;
            			div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            			document.getElementById('notif-' + inName).appendChild(div);
        			}
        			return false;
    			}
    		}
			else {
    			document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
    	
    	function validatePhoto() {
    		var inName = "photo";
			var kosong = document.getElementById('isinotif-' + inName);
			
			if (document.getElementById(inName).value!="") {
      			document.getElementById(inName).style.borderWidth="thin";
    			document.getElementById(inName).style.borderColor="#66afe9";//and you want to remove invalid style
				if (kosong!=null) {
            		kosong.innerHTML = '';
        		}	
        		return true;
    		}
			else {
        		document.getElementById(inName).style.borderColor="red";
        		document.getElementById(inName).style.borderWidth="medium";
        		if (kosong!=null) {

        		}
        		else {
            		var div = document.createElement('div');
           			div.id = 'isinotif-' + inName;
            		div.innerHTML = '<div style="color:red;">* Please fill the field correctly !</div>';
            		document.getElementById('notif-' + inName).appendChild(div);
        		}
        		return false;
    		}
    	}
    	
    	function validatePhotoEdit() {
    		var inName = "photo";
			var kosong = document.getElementById('isinotif-' + inName);
			
			return true;
    	}